const sanitizeHTML = require('sanitize-html');

module.exports = function(url,callback){
  const mongoose = require('mongoose');
  mongoose.connect(url,callback);

  const Messages = mongoose.model(
	'Messages',
	{ username: String, text: String }
  );

  return {

    create:function(newMessage,callback){	
	var messages = new Messages(newMessage);
	messages.save(callback);
    },

    read:function(id,callback){
	Messages.findById(id, callback);
    },

    readUsername:function(username,callback){
      callback();
    },

    readAll:function(callback){
      callback();
    },

    update:function(id,updatedMessage,callback){
      callback();
    },

    delete:function(id,callback){
      callback();
    },

    deleteAll:function(callback){
       Messages.remove({},callback);
    },

    disconnect:function(){
      mongoose.disconnect();
    }

  };
};
